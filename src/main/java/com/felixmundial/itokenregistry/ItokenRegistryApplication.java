package com.felixmundial.itokenregistry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ItokenRegistryApplication {

    public static void main(String[] args) {
        SpringApplication.run(ItokenRegistryApplication.class, args);
    }

}
